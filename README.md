# mozzarella

PHP Mozilla add-ons scraper.

scraper.php scraps the addons from addons.mozilla.org and store them into a MySQL Database.

Contains a website to display the free addons and a link to download and install into a Firefox-based browser.

Currently, the free AND non-free addons are displayed!

Source code is in develop branch

# installation

Tested with PHP 8.1, MySQL 15.1, Apache 2.4.52 (Ubuntu)

First restore the mozzarella.sql database

The database already contains all the Firefox extensions (free and non-free). That does not include themes, only extensions.
Running scraper.php will ADD to the existing database (making duplicates)

To run the main website, go to index.php


## License
The project is free and open-source.

## Project status

The project is built during my free time. Feel free to contribute to it.

# screenshots


![image-1.png](./image-1.png)

![image-2.png](./image-2.png)


![image.png](./image.png)
